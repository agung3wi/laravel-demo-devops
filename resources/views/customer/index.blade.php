@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Daftar Pelanggan
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>No Telp </th>
                            <th>Alamat</th>
                        </tr>
                        @foreach ($customerList as $customer)
                        <tr>
                            <td>{{$customer->customer_code}}</td>
                            <td>{{$customer->customer_name}}</td>
                            <td>{{$customer->phone}}</td>
                            <td>{{$customer->address}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection